import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIAD${(page.page - 1) * page.size},${page.size}` : '/?LIAD',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:ad',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:ad',
    method: 'post',
    data
  })
}

export function getImagesList(page) {
  return request({
    url: page ? `/?LIPT${(page.page - 1) * page.size},${page.size}` : '/?LIPT',
    method: 'post'
  })
}
export function updateImages(data) {
  return request({
    url: '/?JSONi:ppt',
    method: 'post',
    data
  })
}
export function delImages(data) {
  return request({
    url: '/?JSONd:ppt',
    method: 'post',
    data
  })
}

export function postMp4(url) {
  return request({
    url: '/?HMP4' + url,
    method: 'post'
  })
}
