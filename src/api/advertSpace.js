import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIPS${(page.page - 1) * page.size},${page.size}` : '/?LIPS',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:adc',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:adc',
    method: 'post',
    data
  })
}
