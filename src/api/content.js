import request from '@/utils/request'

export function getAdvertPage() {
  return request({
    url: '/?LIAD',
    method: 'post'
  })
}
export function getAdvertSpacePage() {
  return request({
    url: '/?LIPS',
    method: 'post'
  })
}
export function update(data) {
  return request({
    url: '/?JSONi:ad',
    method: 'post',
    data
  })
}
export function deleteAdvert(data) {
  return request({
    url: '/?JSONd:ad',
    method: 'post',
    data
  })
}
export function updateAdvertSpace(data) {
  return request({
    url: '/?JSONi:adc',
    method: 'post',
    data
  })
}
export function deleteAdvertSpace(data) {
  return request({
    url: '/?JSONd:adc',
    method: 'post',
    data
  })
}
