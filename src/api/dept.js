import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIRM${(page.page - 1) * page.size},${page.size}` : '/?LIRM',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:room',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:room',
    method: 'post',
    data
  })
}
