import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIDC${(page.page - 1) * page.size},${page.size}` : '/?LIDC',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:doctor',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:doctor',
    method: 'post',
    data
  })
}
