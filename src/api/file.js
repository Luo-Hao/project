import request from '@/utils/request'

const guid = (file) => {
  const arr = file.split('.')
  const type = arr[arr.length - 1]
  return arr[0] + '_' + new Date().getTime() + '.' + type.toLowerCase()
}
export function uploadFile(query, param) {
  return request({
    url: `/nmgpic/${guid(param.file.name)}`,
    method: 'put',
    data: query,
    // 获取上传的进度
    onUploadProgress: event => {
      param.file.percent = event.loaded / event.total * 100
      // 此处调用处理中
      param.onProgress(param.file)
    }
  })
}

