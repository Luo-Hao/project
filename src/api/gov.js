import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIGV${(page.page - 1) * page.size},${page.size}` : '/?LIGV',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:gov',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:gov',
    method: 'post',
    data
  })
}
