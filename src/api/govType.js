import request from '@/utils/request'

export function getList(page) {
  return request({
    url: page ? `/?LIGC${(page.page - 1) * page.size},${page.size}` : '/?LIGC',
    method: 'post'
  })
}

export function update(data) {
  return request({
    url: '/?JSONi:govc',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/?JSONd:govc',
    method: 'post',
    data
  })
}
