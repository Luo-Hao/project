import request from '@/utils/request'

export function getIndexData() {
  return request({
    url: '/?MENU',
    method: 'post'
  })
}
export function getIndexEcharts() {
  return request({
    url: '/?ECHT',
    method: 'post'
  })
}
export function getApplication() {
  return request({
    url: '/?LIAS',
    method: 'post'
  })
}
export function setApplication(data) {
  return request({
    url: '/?JSONi:appset',
    method: 'post',
    data
  })
}
