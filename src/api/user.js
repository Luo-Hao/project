import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/?LGIN',
    method: 'post',
    data: data
  })
}
export function getUserList(page) {
  return request({
    url: page ? `/?LIUS${(page.page - 1) * page.size},${page.size}` : '/?LIUS',
    method: 'post'
  })
}
export function getInterList(page) {
  return request({
    url: page ? `/?LIWT${(page.page - 1) * page.size},${page.size}` : '/?LIWT',
    method: 'post'
  })
}
export function getMessageList(page) {
  return request({
    url: page ? `/?LIMG${(page.page - 1) * page.size},${page.size}` : '/?LIMG',
    method: 'post'
  })
}
export function updateUser(data) {
  return request({
    url: '/?JSONi:user',
    method: 'post',
    data
  })
}
export function updateInter(data) {
  return request({
    url: '/?JSONi:water',
    method: 'post',
    data
  })
}
export function updateMessage(data) {
  return request({
    url: '/?JSONi:message',
    method: 'post',
    data
  })
}
export function delUser(data) {
  return request({
    url: '/?JSONd:user',
    method: 'post',
    data
  })
}
export function delInter(data) {
  return request({
    url: '/?JSONd:water',
    method: 'post',
    data
  })
}
export function delMessage(data) {
  return request({
    url: '/?JSONd:messageUI',
    method: 'post',
    data
  })
}
