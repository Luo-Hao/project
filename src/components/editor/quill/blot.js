import Quill from 'quill'
const BlockEmbed = Quill.import('blots/block/embed')

class ImageBlot extends BlockEmbed {
  static create(value) {
    const node = super.create()
    node.setAttribute('src', value.url)
    node.setAttribute('controls', value.controls)
    node.setAttribute('width', value.width)
    node.setAttribute('height', value.height)
    return node
  }

  static value(node) {
    return {
      url: node.getAttribute('src'),
      controls: node.getAttribute('controls'),
      width: node.getAttribute('width'),
      height: node.getAttribute('height')
    }
  }
}
ImageBlot.blotName = 'image'
ImageBlot.tagName = 'img'

class VideoBlot extends BlockEmbed {
  static create(value) {
    const node = super.create(value)
    node.setAttribute('src', value)
    node.setAttribute('width', '100%')
    node.setAttribute('controls', 'controls')
    return node
  }

  static value(node) {
    return {
      url: node.getAttribute('src'),
      controls: node.getAttribute('controls'),
      width: node.getAttribute('width'),
      height: node.getAttribute('height')
    }
  }
}
VideoBlot.blotName = 'video'
VideoBlot.tagName = 'Video'
VideoBlot.className = 'ql-video'

export { ImageBlot, VideoBlot }
