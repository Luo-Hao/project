import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: '主页',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '主页', icon: 'el-icon-s-home' }
    }]
  },
  {
    path: '/application',
    component: Layout,
    children: [
      {
        path: 'index',
        name: '应用设置',
        component: () => import('@/views/application/index'),
        meta: { title: '应用设置', icon: 'el-icon-s-tools' }
      }
    ]
  },
  {
    path: '/content',
    component: Layout,
    redirect: '/content/advert',
    name: '内容管理',
    meta: { title: '内容管理', icon: 'el-icon-s-claim' },
    children: [
      {
        path: 'advert',
        name: '广告管理',
        component: () => import('@/views/contentManage/advertManage/index'),
        meta: { title: '广告管理', icon: 'el-icon-picture' }
      },
      {
        path: 'images',
        name: '组图管理',
        component: () => import('@/views/contentManage/imagesManage/index'),
        meta: { title: '组图管理', icon: 'el-icon-picture' }
      },
      {
        path: 'advertspace',
        name: '广告位管理',
        component: () => import('@/views/contentManage/advertSpaceManage/index'),
        meta: { title: '广告位管理', icon: 'el-icon-s-finance' }
      },
      {
        path: 'gadvertspace',
        name: '游戏广告位管理',
        component: () => import('@/views/contentManage/gameAdvertSpaceManage/index'),
        meta: { title: '游戏广告位管理', icon: 'el-icon-s-marketing' }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    redirect: '/user/list',
    name: '用户管理',
    meta: { title: '用户管理', icon: 'el-icon-s-custom' },
    children: [
      {
        path: '/user/list',
        name: '用户列表',
        component: () => import('@/views/userManage/userList/index'),
        meta: { title: '用户列表', icon: 'el-icon-s-check' }
      },
      {
        path: '/user/flow',
        name: '积分流水',
        component: () => import('@/views/userManage/integralFlow/index'),
        meta: { title: '积分流水', icon: 'el-icon-s-order' }
      },
      {
        path: '/user/message',
        name: '在线留言',
        component: () => import('@/views/userManage/message/index'),
        meta: { title: '在线留言', icon: 'el-icon-s-comment' }
      }
    ]
  },
  {
    path: '/government',
    component: Layout,
    redirect: '/government/manage',
    name: '政务工作',
    meta: { title: '政务工作', icon: 'el-icon-s-management' },
    children: [
      {
        path: '/government/manage',
        name: '政务管理',
        component: () => import('@/views/governmentManage/manage/index'),
        meta: { title: '政务管理', icon: 'el-icon-s-ticket' }
      },
      {
        path: '/government/classify',
        name: '政务分类',
        component: () => import('@/views/governmentManage/classify/index'),
        meta: { title: '政务分类', icon: 'el-icon-s-unfold' }
      }
    ]
  },
  {
    path: '/clinic',
    component: Layout,
    redirect: '/clinic/dept',
    name: '卫生所',
    meta: { title: '卫生所', icon: 'el-icon-s-cooperation' },
    children: [
      {
        path: '/clinic/doctor',
        name: '医生管理',
        component: () => import('@/views/clinicManage/doctor/index'),
        meta: { title: '医生管理', icon: 'el-icon-user-solid' }
      },
      {
        path: '/clinic/dept',
        name: '科室管理',
        component: () => import('@/views/clinicManage/dept/index'),
        meta: { title: '科室管理', icon: 'el-icon-s-grid' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
